//
// Created by reffya on 05.11.22.
//

#include "MPI_HELPER.h"
#include <mpi.h>
#

void sendMsg(Message msg, int dest){
    shared_ptr<vector<int>> serialized = msg.serialize();
    int* toSend = serialized->data();
    int length = serialized->size();
    MPI_Send(toSend,length,MPI_INT,dest,0,MPI_COMM_WORLD);
}

Message receiveMsg(int dimensions){
    int length = 6 + dimensions;
    MPI_Status status;
    int receiver[length];
    MPI_Recv(receiver,length,MPI_INT,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&status);

    vector<int> msg(0,0);
    for (int i = 0; i < length; ++i) {
        msg.push_back(receiver[i]);
    }
    Message res = Message::deserialize(make_shared<vector<int>>(msg));
    return res;
}