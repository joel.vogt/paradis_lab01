//
// Created by reffya on 05.11.22.
//

#include <iostream>
#include "Worker.h"
#include "Message.h"
#include "MPI_HELPER.h"

Worker::Worker(int id, int dimension, int managerId) : id(id), dimension(dimension), managerId(managerId){
    isEnd = false;
}

bool isSafe(shared_ptr<vector<vector<int>>> board,
            int row, int col)
{
    int i, j;
    int N = board->size();

    /* Check this row on left side */
    for (i = 0; i < col; i++)
        if (board->at(row)[i])
            return false;

    /* Check upper diagonal on left side */
    for (i = row, j = col; i >= 0 && j >= 0; i--, j--)
        if (board->at(i)[j])
            return false;

    /* Check lower diagonal on left side */
    for (i = row, j = col; j >= 0 && i < N; i++, j--)
        if (board->at(i)[j])
            return false;

    return true;
}

int Worker::solveNQUtil(shared_ptr<vector<vector<int> >> board, int col, int endCol)
{
    int res = 0;
    int N = board->size();
    if (col == N) {
        return 1;
    }


    for (int i = 0; i < N; i++) {
        if (isSafe(board, i, col)) {
            board->at(i)[col] = 1;

            if(col == endCol){
                int end = N;
                shared_ptr<Task> task = make_shared<Task>(Task(make_shared<vector<vector<int>>>(*board), col+1, end));
                Message toSend = Message(SEND_WORK,task,id);
                sendMsg(toSend,managerId);
            } else {
                res += solveNQUtil(board, col + 1, endCol);
            }
            board->at(i)[col] = 0;
        }
    }

    return res;
}

void Worker::start(){
    run();
}

void Worker::handleMsg(const Message& msg){
    switch(msg.type){
        case SEND_WORK: {
            int res = solveNQUtil(msg.task->board, msg.task->startCol, msg.task->endCol);
            if (res != 0) {
                Message toSend = Message(SEND_RESULT,dimension, res, id);
                sendMsg(toSend, managerId);
            }
            break;
        }
        case SEND_TERMINATE: {
                isEnd = true;
                break;
            }
        default: {
            cout << "received msg of type " << msg.type << endl;
        }


    }

}

void Worker::run(){
    while(!isEnd){
        // ask for work
        Message ask = Message(ASK_WORK,dimension,0,id);
        sendMsg(ask,managerId);
        // wait for answer
        Message msg = receiveMsg(dimension);

        // treat answer
        handleMsg(msg);
    }
}



