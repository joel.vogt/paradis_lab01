//
// Created by reffya on 05.11.22.
//

#ifndef PARADIS_LAB01_MANAGER_H
#define PARADIS_LAB01_MANAGER_H


#include <queue>
#include <memory>
#include "Task.h"
#include "Message.h"

using namespace std;

class Manager {
protected:
    bool isEnd;
    queue<shared_ptr<Task>> workQueue;
    queue<int> process_waiting_list;
    int comm_size;
    int dimension;
    int res;
    int id;
public:
    Manager(int comm_size, int dimension, int id);
    void start();

    void run();

    void handleMsg(Message msg);

    void terminate();
};


#endif //PARADIS_LAB01_MANAGER_H
