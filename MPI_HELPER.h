//
// Created by reffya on 05.11.22.
//

#ifndef PARADIS_LAB01_MPI_HELPER_H
#define PARADIS_LAB01_MPI_HELPER_H

#include "Message.h"

void sendMsg(Message msg, int dest);
Message receiveMsg(int dimensions);

#endif //PARADIS_LAB01_MPI_HELPER_H
