//
// Created by reffya on 05.11.22.
//

#ifndef PARADIS_LAB01_TASK_H
#define PARADIS_LAB01_TASK_H
#include <vector>
#include <memory>

using namespace std;

class Task {
public:
    shared_ptr<vector<vector<int> >> board;
    int startCol;
    int endCol;
    Task(shared_ptr<vector<vector<int>>> board, int startCol, int endCol);
    Task();
    ~Task();

    void print();
};


#endif //PARADIS_LAB01_TASK_H
