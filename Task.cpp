//
// Created by reffya on 05.11.22.
//

#include "Task.h"
#include <iostream>
#include <utility>

using namespace std;
Task::Task(shared_ptr<vector<vector<int>>> board, int startCol, int endCol) : board(std::move(board)), startCol(startCol), endCol(endCol) {}

void Task::print(){
    cout << "Task: " << startCol << " " << endCol << endl;
}

Task::Task() {

}

Task::~Task() {

}
