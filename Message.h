//
// Created by reffya on 05.11.22.
//

#ifndef PARADIS_LAB01_MESSAGE_H
#define PARADIS_LAB01_MESSAGE_H

#include <vector>
#include <memory>
#include "Task.h"

using namespace std;

enum MessageType {
    ASK_WORK = 0,
    SEND_WORK = 1,
    SEND_RESULT = 2,
    SEND_TERMINATE = 3
};

class Message {
public:
    MessageType type;
    shared_ptr<Task> task;
    int value;
    int sender;
    Message();
    Message(MessageType type, shared_ptr<Task> task, int sender);
    Message(MessageType type, int n, int value, int sender);
    Message(MessageType type, shared_ptr<Task> task, int value, int sender);
    shared_ptr<vector<int>> serialize();
    string printMsg();

    static Message deserialize(const shared_ptr<vector<int>>& input);

};


#endif //PARADIS_LAB01_MESSAGE_H
