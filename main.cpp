/* C/C++ program to solve N Queen Problem using
backtracking */

#include <vector>
#include <iostream>
#include <mpi.h>
#include <chrono>
#include "Manager.h"
#include "Worker.h"

#define BOARD_SIZE 7

using namespace std;

bool isSafes(shared_ptr<vector<vector<int>>> board,
            int row, int col)
{
    int i, j;
    int N = board->size();

    /* Check this row on left side */
    for (i = 0; i < col; i++)
        if (board->at(row)[i])
            return false;

    /* Check upper diagonal on left side */
    for (i = row, j = col; i >= 0 && j >= 0; i--, j--)
        if (board->at(i)[j])
            return false;

    /* Check lower diagonal on left side */
    for (i = row, j = col; j >= 0 && i < N; i++, j--)
        if (board->at(i)[j])
            return false;

    return true;
}

int solveNQUtil(shared_ptr<vector<vector<int> >> board, int col)
{
    int res = 0;
    int N = board->size();
    if (col == N) {
        return 1;
    }
    for (int i = 0; i < N; i++) {
        if (isSafes(board, i, col)) {
            board->at(i)[col] = 1;
            res += solveNQUtil(board, col + 1);
            board->at(i)[col] = 0;
        }
    }

    return res;
}

// Driver Code
int main(int argc, char *argv[])
{



    int nprocs, myself;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myself);
    int n = 17;
    if(myself == 0){
        auto start = std::chrono::system_clock::now();

        Manager manager = Manager(nprocs,n,myself);
        manager.start();
        auto passedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
        cout << "FOR N=" << n << " WITH " << nprocs << " CORES" << endl
        << "finished in " << static_cast<float>(passedTime.count())/1000 << "s" << endl;
    } else {
        Worker worker = Worker(myself,n,0);
        worker.start();
    }



    MPI_Finalize();





    return 0;
}