//
// Created by reffya on 05.11.22.
//

#include "Manager.h"
#include "Message.h"
#include "MPI_HELPER.h"
#include <mpi.h>

using namespace std;

Manager::Manager(int comm_size, int dimension, int id) : comm_size(comm_size), dimension(dimension), id(id) {
    isEnd = false;
    res = 0;
}


void Manager::start() {
    // create initial workload
    for (int i = 0; i < dimension; ++i) {
        vector<vector<int> > board(dimension, vector<int>(dimension, 0));
        board[i][0] = 1;
        // push work assignments in work queue
        workQueue.push(make_shared<Task>(Task(make_shared<vector<vector<int> >>(board),1,1)));

    }
    // then begin main execution
        run();

}

void Manager::handleMsg(Message msg){
    switch(msg.type){
        case ASK_WORK: {
                process_waiting_list.push(msg.sender);
            break;
        }
        case SEND_WORK: {
                workQueue.push(msg.task);
            break;
        }
        case SEND_RESULT: {
            res += msg.value;
            break;
        }
        case SEND_TERMINATE: {
            cout << "worker process should never send terminate msg" << endl;
            break;
        }

    }
}

void Manager::terminate(){
    isEnd = true;
    for (int i = 1; i < comm_size; ++i) {
        Message terminate = Message(SEND_TERMINATE,dimension,0,id);
        sendMsg(terminate,i);
    }
}

void Manager::run() {
    long unsigned int maxQueue = 0;
    while(!isEnd) {
            Message msg = receiveMsg(dimension);
            handleMsg(msg);

            while(!process_waiting_list.empty() && !workQueue.empty()){
                shared_ptr<Task> task = workQueue.front();
                sendMsg(Message(SEND_WORK,task,id),process_waiting_list.front());
                workQueue.pop();
                process_waiting_list.pop();
            }

            if(workQueue.size() > maxQueue) maxQueue= workQueue.size();
        if(workQueue.empty() && process_waiting_list.size() == static_cast<long unsigned int>(comm_size -1)){
            terminate();
        }
    }
   // cout << "final result is : " << res << endl;
   // cout << "max load on workqueue was " << maxQueue << " tasks" << endl;
}