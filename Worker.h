//
// Created by reffya on 05.11.22.
//

#ifndef PARADIS_LAB01_WORKER_H
#define PARADIS_LAB01_WORKER_H


#include <vector>
#include <queue>
#include "Task.h"
#include "Message.h"

using namespace std;

class Worker {
protected:
    int id;
    int dimension;
    int managerId;
    queue<shared_ptr<Task>> taskList;
    bool isEnd;
public:
    explicit Worker(int id, int dimension, int managerId);
    void start();

    int solveNQUtil(shared_ptr<vector<vector<int>>> board, int beginCol, int endCol);

    void run();

    void handleMsg(const Message& msg);
};


#endif //PARADIS_LAB01_WORKER_H
