#!/bin/bash
for i in k8s-1,k8s-2 k8s-1,k8s-2,k8s-3 k8s-1,k8s-2,k8s-3,k8s-4 k8s-1,k8s-2,k8s-3,k8s-4,k8s-5 k8s-1,k8s-2,k8s-3,k8s-4,k8s-5,k8s-6 k8s-1,k8s-2,k8s-3,k8s-4,k8s-5,k8s-6,k8s-7 k8s-1,k8s-2,k8s-3,k8s-4,k8s-5,k8s-6,k8s-7,k8s-8
do
  mpirun   -host $i ./paradis_lab01
  wait
done