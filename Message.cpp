//
// Created by reffya on 05.11.22.
//

#include "Message.h"

#include <utility>
#include <string>
#include <sstream>
#include <iostream>

const char DELIMITER = '/';

Message::Message(MessageType type, shared_ptr<Task> task, int sender) : type(type), task(std::move(task)),
sender(sender) {
value = 0;
}

Message::Message(MessageType type, shared_ptr<Task> task, int value, int sender) : type(type),
task(std::move(task)), value(value), sender(sender){}


Message::Message(MessageType type, int n, int value, int sender) : type(type), value(value), sender(sender) {
    vector<vector<int> > board(n, vector<int>(n, 0));
    task = make_shared<Task>(Task(make_shared<vector<vector<int>>>(board),0,0));
}

shared_ptr<vector<int>> Message::serialize() {
    vector<int> output(6 + task->board->size(),0);
    output[0] = type;
    output[1] = task->startCol;
    output[2] = task->endCol;
    output[3] = value;
    output[4] = task->board->size();
    output[5] = sender;

    for (long unsigned int i = 0; i < task->board->size() ; ++i) {
        int res = -1;
        for (long unsigned int j = 0; j < task->board->size(); ++j) {
            if(task->board->at(i)[j] == 1){
                res =  i * task->board->size() + j;
            }
        }
        output[6 + i] = res;
    }


    return make_shared<vector<int>>(output);
}

string getMsgType(int type){
    switch (type) {
        case 0 : return "ASK_WORK";
        case 1 : return "SEND_WORK";
        case 2 : return "SEND_RESULT";
        case 3 : return "SEND_TERMINATE";
        default : return "UNKNOWN";
    }
}

string Message::printMsg(){
    stringstream s;
    s << getMsgType(type) << "/";
    s << task->startCol << "/";
    s << task->endCol << "/";
    s << "[";
    for (long unsigned int i = 0; i < task->board->size() ; ++i) {
        for (long unsigned int j = 0; j < task->board->size(); ++j) {
            if((*task->board)[i][j] == 1){
                s << i * task->board->size() + j << DELIMITER;
            }
        }
    }
    s << "]";
    return s.str();
}

Message Message::deserialize(const shared_ptr<vector<int>>& input) {

        auto type = static_cast<MessageType>(input->at(0));
        int starCol = input->at(1);
        int endCol = input->at(2);
        int value = input->at(3);
        int size = input->at(4);
        int sender = input->at(5);

        vector<vector<int> > board(size, vector<int>(size, 0));


        for (long unsigned int i = 6; i < input->size(); ++i) {
            int queen = input->at(i);
            if(queen == -1) continue;
            int line = queen / size;
            int col = queen % size;
            board[line][col] = 1;
        }
        return {type, make_shared<Task>(Task(make_shared<vector<vector<int> >>(board),starCol,
                                             endCol)),value,sender};

}


